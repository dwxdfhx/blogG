package main

import (
	"gitee.com/johng/gf/g"
	"gitee.com/johng/gf/g/net/ghttp"
	_ "dw.com/blogG/controllerG"
	"gitee.com/johng/gf/g/os/glog"
)

func main() {
	s := g.Server();
	s.BindHandler("/", func(r *ghttp.Request) {
		r.Response.Write("blogG!")
	})

	//静态资源目录设置
	s.SetIndexFolder(true)
	s.SetServerRoot("views")

	//设置端口
	s.SetPort(81, 82)

	glog.SetDebug(true)
	glog.SetPath("./zlogs/")
	glog.Debug("blogG start...")
	glog.Error("test error")
	glog.Info("test info")

	s.SetAccessLogEnabled(true)
	s.Run()
}
