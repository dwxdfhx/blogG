package main

import (
	"strings"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"fmt"
	"gitee.com/johng/gf/g"
	"encoding/base64"
	"time"
	"crypto/aes"
	"crypto/cipher"
	"net/http"
	"io/ioutil"
)

func getTimeDaoqi() (bool) {
	g.Config().SetPath("ui/")

	timepass := g.Config().GetString("info2", "info.yml")
	bytesPass, err := base64.StdEncoding.DecodeString(timepass)

	var data []byte = []byte(bytesPass)
	tpass, err := AesDecrypt(data, aeskey)
	if err != nil {
		fmt.Println(err)
	}
	time1 := string(tpass)
	return isDaoqi(time1)
}
func getUrlGoogle() (string, bool) {
	var rebackurl string
	g.Config().SetPath("ui/")

	urlpass := g.Config().GetString("info1", "info.yml")
	bytesPass, err := base64.StdEncoding.DecodeString(urlpass)

	var data []byte = []byte(bytesPass)
	tpass, err := AesDecrypt(data, aeskey)
	if err != nil {
		fmt.Println(err)
		return "", false
	}
	rebackurl = string(tpass)
	return rebackurl, true;
}
func main() {
	/*var le *walk.LineEdit*/

	var wv *walk.WebView
	url, canget := getUrlGoogle()
	var daoqi bool
	if (!canget) {
		daoqi = true;
	} else {
		daoqi = getTimeDaoqi()
	}
	if (daoqi) {
		MainWindow{
			Icon:    Bind("'../ui/1.ico'"),
			Title:   "已经到期或者当前无法连接网络",
			MinSize: Size{1366, 768},
			Layout:  VBox{MarginsZero: true},
			Children: []Widget{
				/*LineEdit{
					AssignTo: &le,
					Text:     Bind("wv.URL"),
					OnKeyDown: func(key walk.Key) {
						if key == walk.KeyReturn {
							wv.SetURL(le.Text())
						}
					},
				},*/
				WebView{
					AssignTo: &wv,
					Name:     "wv",
					URL:      "https://baidu.com",
				},
			},
			Functions: map[string]func(args ...interface{}) (interface{}, error){
				"icon": func(args ...interface{}) (interface{}, error) {
					if strings.HasPrefix(args[0].(string), "https") {
						return "check", nil
					}

					return "stop", nil
				},
			},
		}.Run()
	} else {
		MainWindow{
			Icon:    Bind("'../ui/1.ico'"),
			Title:   "google专用",
			MinSize: Size{1366, 768},
			Layout:  VBox{MarginsZero: true},
			Children: []Widget{
				/*LineEdit{
					AssignTo: &le,
					Text:     Bind("wv.URL"),
					OnKeyDown: func(key walk.Key) {
						if key == walk.KeyReturn {
							wv.SetURL(le.Text())
						}
					},
				},*/
				WebView{
					AssignTo: &wv,
					Name:     "wv",
					URL:      url,
				},
			},
			Functions: map[string]func(args ...interface{}) (interface{}, error){
				"icon": func(args ...interface{}) (interface{}, error) {
					if strings.HasPrefix(args[0].(string), "https") {
						return "check", nil
					}

					return "stop", nil
				},
			},
		}.Run()
	}
}

//获取相差时间
func isDaoqi(start_time string) bool {
	t1, _ := time.Parse("2006-01-02 15:04:05", start_time)

	t2response, _ := http.Get("http://cgi.im.qq.com/cgi-bin/cgi_svrtime")
	defer t2response.Body.Close()
	t2body, _ := ioutil.ReadAll(t2response.Body)
	a := string(t2body)
	a = strings.Replace(a, "\n", "", -1)

	t2, _ := time.Parse("2006-01-02 15:04:05", a)

	timeok := t2.After(t1)
	println("time", t1.String(), t2.String(), timeok)
	return timeok
}

func
PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

func
AesDecrypt(crypted, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	return origData, nil
}

var aeskey = []byte("321423u9y8d2fwfl")
