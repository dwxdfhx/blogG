package main

import (
	"fmt"
	"crypto/cipher"
	"crypto/aes"
	"bytes"
	"encoding/base64"
	"time"
	"net/http"
	"io/ioutil"
	"strings"
)

func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

func AesEncrypt(origData, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	origData = PKCS5Padding(origData, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize])
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}

func AesDecrypt(crypted, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	return origData, nil
}

var aeskey = []byte("321423u9y8d2fwfl")
//获取相差时间
func isDaoqi(start_time string) bool {
	t1, _ := time.Parse("2006-01-02 15:04:05", start_time)

	t2response, _ := http.Get("http://cgi.im.qq.com/cgi-bin/cgi_svrtime")
	defer t2response.Body.Close()
	t2body, _ := ioutil.ReadAll(t2response.Body)
	a := string(t2body)
	a = strings.Replace(a, "\n", "", -1)

	t2, _ := time.Parse("2006-01-02 15:04:05", a)
	println("time", t1.String(), t2.String())
	timeok := t1.After(t2)
	return timeok
}
func main() {
	var1 := "https://g.laji.cx/"
	var1 = "2018-07-30 23:54:19"
	a := isDaoqi("2018-07-05 00:00:25")
	fmt.Println(a)
	a = isDaoqi("2018-07-03 00:00:25")
	fmt.Println(a)
	pass := []byte(var1)
	xpass, err := AesEncrypt(pass, aeskey)
	if err != nil {
		fmt.Println(err)
		return
	}

	pass64 := base64.StdEncoding.EncodeToString(xpass)
	fmt.Printf("加密后:%v\n", pass64)

	bytesPass, err := base64.StdEncoding.DecodeString(pass64)
	if err != nil {
		fmt.Println(err)
		return
	}

	tpass, err := AesDecrypt(bytesPass, aeskey)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("解密后:%s\n", tpass)
}
