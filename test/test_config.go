package main

import (
	"gitee.com/johng/gf/g"
	"fmt"
)

func main() {
	fmt.Println("config")
	//项目根目录下  /config
	//g.Config()方式
	g.Config().SetPath("test/")
	configpath := g.Config().GetFilePath("info.yml")

	fmt.Println("path", configpath)

	urlpass := g.Config().GetString("info1", "../test/info.yml")
	fmt.Println("url:", urlpass)

	ports := g.Config().Get("server.ports")
	info := g.Config().GetString("info")
	fmt.Println(ports, info)

}
