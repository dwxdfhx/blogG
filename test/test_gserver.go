package main

import (
	"gitee.com/johng/gf/g"
	"gitee.com/johng/gf/g/net/ghttp"
)

func main() {
	s := g.Server();
	//通过回调函数注册控制器
	s.BindHandler("/", func(r *ghttp.Request) {
		r.Response.Write("body info")
	})

	//绑定域名 只能通过特定域名访问
	s.Domain("127.0.0.webview.exe.manifest,localhost").BindHandler("/url", func(r *ghttp.Request) {
		ip1 := r.GetClientIp()
		println("ip:", ip1)
		r.Response.Write("url")
	})
	//静态资源目录设置
	s.SetIndexFolder(true)
	s.SetServerRoot("views")

	//设置端口
	s.SetPort(81, 82)

	s.Run()
}
