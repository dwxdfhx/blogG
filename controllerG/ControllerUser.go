package controllers

/**
   main程序中通过导入当前包,完成所有controller init初始化加载
	_ "dw.com/ginBlog/controllers"
 */
import (
	"gitee.com/johng/gf/g/frame/gmvc"
	"gitee.com/johng/gf/g/net/ghttp"
	"time"
)

type ControllerUser struct {
	gmvc.Controller
}

//给User模块绑定路由 每个具体方法驼峰规则ShowInfo show-info
func init() {
	ghttp.GetServer().BindController("/user", &ControllerUser{})
}

func (c *ControllerUser) Name() {
	cookie := c.Cookie;
	cookie.Set("server-cookie-username", "name1"+time.Now().String())
	c.Response.Write("name :  ", c.Request.Get("name"))
}

func (c *ControllerUser) Age() {
	c.Response.Write("age :  ", c.Request.Get("age"))
}

func (c *ControllerUser) ShowInfo() {
	c.Response.Write("show info : user info")
}
func (c *ControllerUser) Redirect() {
	c.Response.RedirectTo("http://baidu.com")
}
