package controllers

import (
	"gitee.com/johng/gf/g/frame/gmvc"
	"gitee.com/johng/gf/g/net/ghttp"
)

type ControllerAsk struct {
	gmvc.Controller
}

func init() {
	ghttp.GetServer().BindController("/ask", &ControllerAsk{})
}

func (c *ControllerAsk) Index() {
	if r, err := ghttp.Get("http://localhost:82/user/show-info"); err == nil {
		content := r.ReadAll()
		//string()方法接收二进制流,转字符串
		strcontent := string(content)
		r.Close()
		c.Response.Write("请求成功,返回内容:", strcontent)
	} else {
		c.Response.Write(err.Error())
	}

}

//ask-post
func (c *ControllerAsk) AskPost() {

}
